#include "receiver.hpp"

#include "logging.hpp"

#include "barcodereceiver/barcodereceiver_serial.hpp"
#include "barcodereceiver/barcodereceiver_tcpclient.hpp"
#include "barcodereceiver/barcodereceiver_tcpserver.hpp"

Receiver::Receiver(QObject *parent) : 
    QObject(parent)
  , m_settings(Q_NULLPTR)
  , m_receiving(false)
{

}

void Receiver::start()
{
    this->setReceiving(true);
}

void Receiver::stop()
{
    this->setReceiving(false);
}

Settings *Receiver::settings() const
{
    return m_settings;
}

void Receiver::setSettings(Settings *settings)
{
    if (m_settings != settings)
    {
        m_settings = settings;
        emit settingsChanged();
    }
}

bool Receiver::receiving() const
{
    return m_receiving;
}

void Receiver::setReceiving(bool receiving)
{
    if (m_receiving != receiving)
    {
        m_receiving = receiving;
        emit receivingChanged();
        
        if (m_receiving)
        {
            startInternal();
        }
        else
        {
            stopInternal();
        }
    }
}

void Receiver::startInternal()
{
    if (!m_settings)
    {
        qCWarning(lgReceiver)<<"settings not available";
        return;
    }
    
    Settings::CrInputMode inputMode = m_settings->crInputMode();
    switch (inputMode) {
    case Settings::TcpServer:
        m_codeReceiver = QSharedPointer<Barcodereceiver::CodeReceiver>(
                    new Barcodereceiver::CodeReceiverTcp
                    (
                        m_settings->crTcpServerPort()
                        , m_settings->crMaxBufferSize()
                        , m_settings->crUseEolPattern()
                        , m_settings->crEolPattern()
                        , m_settings->crUseCategoryPattern()
                        , m_settings->crCategoryPattern()
                    )
                );
        break;
        
    case Settings::TcpClient:
        m_codeReceiver = QSharedPointer<Barcodereceiver::CodeReceiver>(
                    new Barcodereceiver::CodeReceiverTcpClient
                    (
                        QHostAddress(m_settings->crTcpClientHostAddress())
                        , m_settings->crTcpClientPort()
                        , m_settings->crMaxBufferSize()
                        , m_settings->crUseEolPattern()
                        , m_settings->crEolPattern()
                        , m_settings->crUseCategoryPattern()
                        , m_settings->crCategoryPattern()
                    )
                );
        break;
        
    case Settings::Serial:
        m_codeReceiver = QSharedPointer<Barcodereceiver::CodeReceiver>(
                    new Barcodereceiver::CodeReceiverSerial
                    (
                        m_settings->crSerialPort()
                        , m_settings->crMaxBufferSize()
                        , m_settings->crUseEolPattern()
                        , m_settings->crEolPattern()
                        , m_settings->crUseCategoryPattern()
                        , m_settings->crCategoryPattern()
                    )
                );
        break;
        
    default:
        m_codeReceiver = QSharedPointer<Barcodereceiver::CodeReceiver>(
                    new Barcodereceiver::CodeReceiver
                    (
                        m_settings->crMaxBufferSize()
                        , m_settings->crUseEolPattern()
                        , m_settings->crEolPattern()
                        , m_settings->crUseCategoryPattern()
                        , m_settings->crCategoryPattern()
                    )
                );
        break;
    }
    
    connect(m_codeReceiver.data(), &Barcodereceiver::CodeReceiver::dataReceived,    this, &Receiver::dataReceived);
    connect(m_codeReceiver.data(), &Barcodereceiver::CodeReceiver::categoryChanged, this, &Receiver::categoryChanged);
}

void Receiver::stopInternal()
{
    m_codeReceiver = QSharedPointer<Barcodereceiver::CodeReceiver>(Q_NULLPTR);
}

QString Receiver::category() const
{
    QString result =  (m_codeReceiver.isNull()) ? "" : m_codeReceiver->category();
    return result;
}

void Receiver::process(const QString &data)
{
    if (! m_codeReceiver.isNull())
    {
        m_codeReceiver->setData(data.toLatin1());
    }
}
