#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QObject>
#include <QSettings>
#include <QSharedPointer>

class Settings : public QObject
{
    Q_OBJECT
    
    // codeReceiver settings
    Q_PROPERTY(int crMaxBufferSize READ crMaxBufferSize WRITE setCrMaxBufferSize NOTIFY crMaxBufferSizeChanged)
    
    Q_PROPERTY(bool crUseEolPattern READ crUseEolPattern WRITE setCrUseEolPattern NOTIFY crUseEolPatternChanged)
    Q_PROPERTY(QString crEolPattern READ crEolPattern WRITE setCrEolPattern NOTIFY crEolPatternChanged)

    Q_PROPERTY(bool crUseCategoryPattern READ crUseCategoryPattern WRITE setCrUseCategoryPattern NOTIFY crUseCategoryPatternChanged)
    Q_PROPERTY(QString crCategoryPattern READ crCategoryPattern WRITE setCrCategoryPattern NOTIFY crCategoryPatternChanged)
    
    Q_PROPERTY(Settings::CrInputMode crInputMode READ crInputMode WRITE setCrInputMode NOTIFY crInputModeChanged)
    
    Q_PROPERTY(quint16 crTcpServerPort READ crTcpServerPort WRITE setCrTcpServerPort NOTIFY crTcpServerPortChanged)
    Q_PROPERTY(QString crTcpClientHostAddress READ crTcpClientHostAddress WRITE setCrTcpClientHostAddress NOTIFY crTcpClientHostAddressChanged)
    Q_PROPERTY(quint16 crTcpClientPort READ crTcpClientPort WRITE setCrTcpClientPort NOTIFY crTcpClientPortChanged)
    Q_PROPERTY(QString crSerialPort READ crSerialPort WRITE setCrSerialPort NOTIFY crSerialPortChanged)
    
    Q_PROPERTY(bool otherAutoType READ otherAutoType WRITE setOtherAutoType NOTIFY otherAutoTypeChanged)
    Q_PROPERTY(quint32 otherDelayAfterFocusLast READ otherDelayAfterFocusLast WRITE setOtherDelayAfterFocusLast NOTIFY otherDelayAfterFocusLastChanged)
    Q_PROPERTY(int otherSleepAfterEol READ otherSleepAfterEol WRITE setOtherSleepAfterEol NOTIFY otherSleepAfterEolChanged)
public:
    Settings(QObject * parent = Q_NULLPTR);
    
    // codeReceiver settings
    
    enum CrInputMode
    {
        KeyboardOnly,
        TcpServer,
        TcpClient,
        Serial
    };
    Q_ENUM(CrInputMode)
    
    int crMaxBufferSize() const;
    void setCrMaxBufferSize(int crMaxBufferSize);
    
    bool crUseEolPattern() const;
    void setCrUseEolPattern(bool crUseEolPattern);
    
    QString crEolPattern() const;
    void setCrEolPattern(const QString &crEolPattern);
    
    bool crUseCategoryPattern() const;
    void setCrUseCategoryPattern(bool crUseCategoryPattern);
    
    QString crCategoryPattern() const;
    void setCrCategoryPattern(const QString &crCategoryPattern);
    
    CrInputMode crInputMode() const;
    void setCrInputMode(const CrInputMode &crInputMode);
    
    quint16 crTcpServerPort() const;
    void setCrTcpServerPort(const quint16 &crTcpServerPort);
    
    
    QString crTcpClientHostAddress() const;
    void setCrTcpClientHostAddress(const QString &crTcpClientHostAddress);
    
    quint16 crTcpClientPort() const;
    void setCrTcpClientPort(const quint16 &crTcpClientPort);
    
    QString crSerialPort() const;
    void setCrSerialPort(const QString &crSerialPort);
    
    QStringList crAvailableSerialPorts()const;
    
    bool otherAutoType() const;
    void setOtherAutoType(bool otherAutoType);
    

    quint32 otherDelayAfterFocusLast()const;
    void setOtherDelayAfterFocusLast(quint32 value);

    int otherSleepAfterEol()const;
    void setOtherSleepAfterEol(int milliseconds);

signals:
    void crMaxBufferSizeChanged();
    void crUseEolPatternChanged();
    void crEolPatternChanged();
    void crUseCategoryPatternChanged();
    void crCategoryPatternChanged();
    void crInputModeChanged();
    void crTcpServerPortChanged();
    void crTcpClientHostAddressChanged();
    void crTcpClientPortChanged();
    void crSerialPortChanged();
    
    void otherAutoTypeChanged();
    void otherDelayAfterFocusLastChanged();
    void otherSleepAfterEolChanged();
    
private:
    QSharedPointer<QSettings> m_settings;
    
    QString m_crTcpClientHostAddress;
    
    
};



#endif // SETTINGS_HPP
