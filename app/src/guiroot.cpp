#include "guiroot.hpp"
#include <QCoreApplication>
#include <QQmlEngine>
#include <QQmlContext>
#include <QtQml>
#include <QFileInfo>
#include <QUrl>
#include <QThread>

#include "lib.utility.utilqmlplugin.hpp"

#include "logging.hpp"
#include "settings.hpp"
#include "receiver.hpp"



void GuiRoot::initOnce()
{
    static bool initialized = false;
    
    if (initialized == false)
    {
        initialized = true;
        
        LibUtilQmlPlugin::registerTypesStatic("Lib.Util");
        qmlRegisterType<Settings>("ThisApp", 1, 0, "Settings");
        qmlRegisterType<Receiver>("ThisApp", 1, 0, "Receiver");
    }
}

GuiRoot::GuiRoot(QObject *parent) : 
    QObject(parent)
{
    initOnce();
    
    QString mainQmlFile = resolveQmlPath("main.qml");
   
    m_appEngine.rootContext()->setContextProperty("HmiRoot", this);
    
    m_appEngine.load(mainQmlFile);
    
    if (m_appEngine.rootObjects().isEmpty())
    {
        qCCritical(lgGuiRoot)<<"Failed to load "<<mainQmlFile;
        QCoreApplication::exit(1);
    }
}

GuiRoot::~GuiRoot()
{
    
}

QString GuiRoot::resolveQmlPath(const QString &file)
{
    QString result = resourcePrefix("qml", file);
    return result;     
}

void GuiRoot::msleep(int milliseconds)
{
    QThread::currentThread()->msleep(milliseconds);
}

QString GuiRoot::resourcePrefix(const QString & category, const QString & file)
{
    QString pathWithoutPrefix = QString("%1/%2")
            .arg(category)
            .arg(file);

    QString result = QString("%1/res/%2")
            .arg(QCoreApplication::applicationDirPath())
            .arg(pathWithoutPrefix);
    
    
    if (!QFileInfo::exists(result))
    {
        qCWarning(lgGuiRoot)<<"not found: "<<result<<"; trying to resolve from source code directory";
        result = QString("%1/%2")
                    .arg(RESOURCE_BASE_DIR)
                    .arg(pathWithoutPrefix);
    }
    
     if (!QFileInfo::exists(result))
     {
         qCWarning(lgGuiRoot())<<"File not found: "<<result;
     }
    
    result = QString("file:///%1").arg(result);
    
    qCDebug(lgGuiRoot)<<"resourcePrefix("<<category<<", "<<file<<") = "<<result;
    return result; 
}

