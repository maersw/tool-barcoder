#include "barcodereceiver_logging.hpp"

namespace Barcodereceiver
{

Q_LOGGING_CATEGORY(lgCR,   "CREC.codeReceiver")
Q_LOGGING_CATEGORY(lgCRT,  "CRTS.codeReceiver.tcpServer")
Q_LOGGING_CATEGORY(lgCRTC, "CRTC.codeReceiver.tcpClient")
Q_LOGGING_CATEGORY(lgCRS,  "CRSE.codeReceiver.serial")
}
