#ifndef CODERECEIVERTCPCLIENT_HPP
#define CODERECEIVERTCPCLIENT_HPP

#include <QObject>
#include <QSharedPointer>
#include <QVector>
#include <QTcpSocket>
#include <QHostAddress>

#include "barcodereceiver.hpp"

namespace Barcodereceiver
{

class CodeReceiverTcpClient : public CodeReceiver
{
    Q_OBJECT
public:
    explicit CodeReceiverTcpClient(
            const QHostAddress & server
            , quint16 port
            , uint32_t maxBufferSize
            , bool useEolPattern
            , const QString & eolPattern
            , bool useCategoryPattern
            , const QString & categoryPattern
            , QObject * parent = Q_NULLPTR
            );
    
signals:
    
public slots:
    
private slots:
    void onConnected();
    void onError(QAbstractSocket::SocketError socketError);
    
private:
    QSharedPointer<QTcpSocket> m_socket;
    
};
}

#endif // CODERECEIVERTCPCLIENT_HPP
