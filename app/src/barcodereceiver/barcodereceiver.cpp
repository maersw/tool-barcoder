/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "barcodereceiver.hpp"
#include <QCoreApplication>

#include "barcodereceiver_logging.hpp"


namespace Barcodereceiver
{

CodeReceiver::CodeReceiver
(
    uint32_t maxBufferSize,
    bool useEolPattern,
    const QString & eolPattern,
    bool useFilePattern,
    const QString & filePattern,
    QObject *parent
) :
    QObject(parent)
  , m_maxBufferSize(maxBufferSize)
  , m_useEolPattern(useEolPattern)
  , m_eolPattern(eolPattern)
  , m_usePatternFile(useFilePattern)
  , m_filePattern(filePattern)
{

}

void CodeReceiver::byteReceived(char byte)
{
    m_buffer.push_back(byte);

    if (m_usePatternFile && m_filePattern.putC(byte))
    {
        //m_buffer.erase(m_buffer.size() - m_filePattern.pattern().size(), QString::npos);
        QString cat = m_buffer.left(m_buffer.size() - m_filePattern.pattern().size());
        setCategory(cat);
        qCDebug(lgCR)<<"Category pattern detected: "<<m_category;
        
        resetBuffer();
    }
    else if (m_eolPattern.putC(byte) || (!m_useEolPattern && !m_usePatternFile))
    {
        flushBuffer();
    }
    else if (static_cast<uint32_t>(m_buffer.size()) > m_maxBufferSize)
    {
        qCWarning(lgCR)<<"Max buffer length reached, flushing...";
        flushBuffer();
    }
    else
    {
        // wait for next character
    }
}



void CodeReceiver::setCategory(const QString &category)
{
    if (m_category != category)
    {
        m_category = category;
        emit categoryChanged();
    }
}

QString CodeReceiver::category() const
{
    return m_category;
}

void CodeReceiver::setData(const QByteArray &data)
{
    for (QByteArray::const_iterator iter = data.begin(); iter != data.end(); ++iter)
    {
        byteReceived(*iter);
    }
}

void CodeReceiver::resetBuffer()
{
    m_buffer.clear();
}

void CodeReceiver::flushBuffer()
{
    qCDebug(lgCR)<<"Flush: "<<m_buffer;
    
    emit dataReceived(m_buffer);
    resetBuffer();
}

}
