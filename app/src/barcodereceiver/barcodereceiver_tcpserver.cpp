/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "barcodereceiver_tcpserver.hpp"

#include <QTcpSocket>
#include <QNetworkInterface>
#include <QLoggingCategory>
#include <QCoreApplication>

#include "barcodereceiver_logging.hpp"

namespace Barcodereceiver
{

CodeReceiverTcp::CodeReceiverTcp(
        quint16 port
        , uint32_t maxBufferSize
        , bool useEolPattern
        , const QString & eolPattern
        , bool useCategoryPattern
        , const QString & categoryPattern
        , QObject * parent
        ):
    CodeReceiver(maxBufferSize, useEolPattern, eolPattern, useCategoryPattern, categoryPattern, parent)
{
    connect(&m_server, &QTcpServer::newConnection, this, &CodeReceiverTcp::onNewConnection);
    
    bool success = m_server.listen(QHostAddress::Any, port);
    
    if (success == false)
    {
        qCCritical(lgCRT)<<"Failed to listen on port "<<port<<"; Error message: "<<m_server.errorString();
        QCoreApplication::instance()->exit(1);
            
    }
    else
    {
        qCInfo(lgCRT)<<"Listening on port "<<m_server.serverPort()<<" on following IP Addresses:";
        
        foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) 
        {
            if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
            {
                qCInfo(lgCRT)<<" "<<address.toString().toLatin1().constData();
            }
        } 
    }
}

void CodeReceiverTcp::onNewConnection()
{
    if (m_server.hasPendingConnections())
    {
        QTcpSocket * client = m_server.nextPendingConnection();
        
        qCInfo(lgCRT)<<"Client connected: "<<client->peerAddress().toString().toLatin1().constData();
                
        connect(client, &QTcpSocket::readyRead, [this, client]()
        {
            this->onDataReceived(client);
        });
        
        connect(client, &QTcpSocket::disconnected, [this, client]()
        {
            qCInfo(lgCRT)<<"Client disconnected: "<<client->peerAddress().toString(); 
            client->deleteLater();
        });
    }
}

void CodeReceiverTcp::onDataReceived(QTcpSocket *client)
{
    
    while (client->bytesAvailable() > 0) {
    
        char byte = 0;
        client->read(&byte, sizeof(byte));
        
        byteReceived(byte);
    }
    
}

}
