/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef CODERECEIVERSERIAL_HPP
#define CODERECEIVERSERIAL_HPP

#include "barcodereceiver.hpp"

#include <QtSerialPort/QSerialPort>

namespace Barcodereceiver
{

class CodeReceiverSerial : public CodeReceiver
{
    Q_OBJECT
public:
    CodeReceiverSerial(
            const QString & comPort
          , uint32_t maxBufferSize
          , bool useEolPattern
          , const QString & eolPattern
          , bool useCategoryPattern
          , const QString & categoryPattern
          , QObject * parent = Q_NULLPTR
            );
    
private slots:
    void onDataReceived();
    
private:
    QSerialPort m_port;
};

}
#endif // CODERECEIVERSERIAL_HPP
