/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef CODERECEIVER_HPP
#define CODERECEIVER_HPP


#include <QString>
#include <fstream>
#include <memory>

#include <QObject>
#include <QSharedPointer>

#include "lib.utility.util_kmp.hpp"

namespace Barcodereceiver
{

class CodeReceiver : public QObject
{
    Q_OBJECT
public:
    explicit CodeReceiver(
            uint32_t maxBufferSize
          , bool useEolPattern
          , const QString & eolPattern
          , bool useFilePattern
          , const QString & filePattern
          , QObject *parent = nullptr
            );
    
    QString category() const;
    
    void setData(const QByteArray & data);
    
signals:
    void dataReceived(QString data);
    void categoryChanged();
    
public slots:
    
protected:
    void byteReceived(char byte);
    
private:
    QString m_buffer;
    uint32_t m_maxBufferSize;
    bool m_useEolPattern;
    libUtil::Kmp<QString> m_eolPattern;
    bool m_usePatternFile;
    libUtil::Kmp<QString> m_filePattern;
    std::shared_ptr<std::ofstream> m_file;
    QString m_category;
    
    
    void setCategory(const QString &category);
    
    void resetBuffer();
    void flushBuffer();
};

}

#endif // CODERECEIVER_HPP
