#include "barcodereceiver_tcpclient.hpp"
#include <QHostAddress>
#include <QCoreApplication>

#include "barcodereceiver_logging.hpp"

namespace Barcodereceiver
{


CodeReceiverTcpClient::CodeReceiverTcpClient(
    const QHostAddress & server
    , quint16 port
    , uint32_t maxBufferSize
    , bool useEolPattern
    , const QString & eolPattern
    , bool useCategoryPattern
    , const QString & categoryPattern
    , QObject * parent
    ) : 
    CodeReceiver(maxBufferSize, useEolPattern, eolPattern, useCategoryPattern, categoryPattern, parent)
{
    m_socket = QSharedPointer<QTcpSocket>(new QTcpSocket);
    
    connect(m_socket.data(), &QTcpSocket::connected, this, &CodeReceiverTcpClient::onConnected);
    connect(m_socket.data(), static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error), this, &CodeReceiverTcpClient::onError);
    
    connect(m_socket.data(), &QTcpSocket::readyRead, [this]()
    {
        while (m_socket->bytesAvailable() > 0) {
        
            char byte = 0;
            m_socket->read(&byte, sizeof(byte));
            
            byteReceived(byte);
        }
        
    });
    
    //connect(m_socket.data(), &QTcpSocket::disconnected, [](){QCoreApplication::instance()->quit();});
    
    connect(m_socket.data(), &QTcpSocket::stateChanged, [=](QAbstractSocket::SocketState state){
        switch (state)
        {
        case QTcpSocket::UnconnectedState:
            m_socket->connectToHost(server, port);
            break;
            
        default:
            break;
        }
        
    });
    
    m_socket->connectToHost(server, port);
    
}

void CodeReceiverTcpClient::onConnected()
{
    
}

void CodeReceiverTcpClient::onError(QAbstractSocket::SocketError socketError)
{
    qCDebug(lgCRTC)<<socketError;
}

}

