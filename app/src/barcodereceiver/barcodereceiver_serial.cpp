/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "barcodereceiver_serial.hpp"

#include <QDebug>
#include <QCoreApplication>

#include "barcodereceiver_logging.hpp"

namespace Barcodereceiver
{

CodeReceiverSerial::CodeReceiverSerial(
        const QString & comPort
      , uint32_t maxBufferSize
      , bool useEolPattern
      , const QString & eolPattern
      , bool useCategoryPattern
      , const QString & categoryPattern
      , QObject * parent
    ):
    CodeReceiver(maxBufferSize, useEolPattern, eolPattern, useCategoryPattern, categoryPattern, parent)
{
    m_port.setPortName(comPort);
    m_port.setBaudRate(QSerialPort::Baud115200);
    
    if (!m_port.open(QIODevice::ReadWrite))
    {
        qCCritical(lgCRS)<<"Failed to open serial port: "<<m_port.errorString();
        QCoreApplication::instance()->exit(1);
    }
    else
    {
        qCDebug(lgCRS)<<"Opened serial port "<<comPort;
        
        connect(&m_port, &QSerialPort::readyRead, this, &CodeReceiverSerial::onDataReceived);
        connect(&m_port, &QSerialPort::errorOccurred, [this](QSerialPort::SerialPortError){
            qCCritical(lgCRS)<<"Error: "<<m_port.errorString(); 
            QCoreApplication::instance()->exit(1);
        });

    }
}

void CodeReceiverSerial::onDataReceived()
{
    while (m_port.bytesAvailable() > 0)
    {
        char byte = 0;
        m_port.read(&byte, sizeof(byte));
        
        byteReceived(byte);
    }
}

}
