#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "guiroot.hpp"
#include "logging.hpp"
#include "lib.utility.log2file.hpp"



int main(int argc, char *argv[])
{
#if DISABLE_QML_CACHE != 0
    qputenv("QML_DISABLE_DISK_CACHE", "1"); 
#endif
    
    libUtil::Log2File::install("Logfile.txt");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    
    QGuiApplication app(argc, argv);
    app.setApplicationDisplayName("Barcoder");
    app.setApplicationName("Barcoder");
    
    qCInfo(lgMain)<<"Version: "<<app.applicationVersion();
    
    
    GuiRoot root;
    Q_UNUSED(root);
    
    return app.exec();
}
