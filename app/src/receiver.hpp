#ifndef RECEIVER_HPP
#define RECEIVER_HPP

#include <QObject>
#include <QSharedPointer>
#include "settings.hpp"

#include "barcodereceiver/barcodereceiver.hpp"

class Receiver : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Settings* settings READ settings WRITE setSettings NOTIFY settingsChanged)
    Q_PROPERTY(bool receiving READ receiving WRITE setReceiving NOTIFY receivingChanged)
    Q_PROPERTY(QString category READ category  NOTIFY categoryChanged)
public:
    explicit Receiver(QObject *parent = nullptr);
    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    
    Settings *settings() const;
    void setSettings(Settings *settings);
    
    bool receiving() const;
    void setReceiving(bool receiving);
    
    QString category() const;
    
    Q_INVOKABLE void process(const QString & data);
    
signals:
    void settingsChanged();
    void receivingChanged();
    void dataReceived(QString data);
    void categoryChanged();
    
public slots:
    
private slots:
    //void onDataReceived(QString data);
    
private:
    Settings * m_settings;
    bool m_receiving;
    QSharedPointer<Barcodereceiver::CodeReceiver> m_codeReceiver;
    
    void startInternal();
    void stopInternal();
    
};

#endif // RECEIVER_HPP
