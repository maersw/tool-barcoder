#ifndef GUIROOT_HPP
#define GUIROOT_HPP

#include <QObject>
#include <QQmlApplicationEngine>



class GuiRoot : public QObject
{
    Q_OBJECT
public:
    explicit GuiRoot(QObject *parent = nullptr);
    virtual ~GuiRoot();
    
    
    Q_INVOKABLE QString resolveQmlPath(const QString & file);
    Q_INVOKABLE void msleep(int milliseconds);
    
signals:
    
public slots:
    
private:
    
    QString resourcePrefix(const QString & category, const QString & file);
    
    QQmlApplicationEngine m_appEngine;
    
    void initOnce();
};

#endif // GUIROOT_HPP
