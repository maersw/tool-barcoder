/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "settings.hpp"

#include <QCoreApplication>
#include <QString>
#include <QSerialPortInfo>


const char * key_cr_maxBufferSize = "cr/maxBufferSize";
int default_cr_maxBufferSize = 128;

const char * key_cr_useEolPattern = "cr/useEolpattern";
bool default_cr_useEolPattern = true;

const char * key_cr_eolPattern = "cr/eolPattern";
const char * default_cr_eol_pattern = "\r\n";

const char * key_cr_useCategoryPattern = "cr/useCategoryPattern";
bool default_cr_useCategoryPattern = true;

const char * key_cr_categoryPattern = "cr/categoryPattern";
const char * default_cr_category_pattern = "=:KAT:\r\n";

const char * key_cr_inputMode = "cr/inputMode";
Settings::CrInputMode default_crInputMode = Settings::KeyboardOnly;

const char * key_cr_tcpServerPort = "cr/tcpServerPort";
quint16 default_cr_tcpServerPort = 2345;

const char * key_cr_tcpClientHostAddress = "cr/tcpClientHostAddress";
QString default_cr_tcpClientHostAddress = "127.0.0.1";

const char * key_cr_tcpClientPort = "cr/tcpClientPort";
quint16 default_cr_tcpClientPort = 2345;

const char * key_cr_serialPort = "cr/serialPort";
QString default_cr_serialPort = "COM1";

const char * key_other_autoType = "other/autoType";
bool default_other_autoType = false;


const char * key_other_delayAfterFocusLast = "other/delayAfterFocusLast";
quint32 default_other_delayAfterFocusLast = 250;

const char * key_other_sleepAfterEol = "other/sleepAfterEol";
int default_other_sleepAfterEol = 50;


Settings::Settings(QObject * parent):
    QObject(parent)
{
    QString settingsFile = QString("%1/settings.ini").arg(QCoreApplication::applicationDirPath());
    
    m_settings = QSharedPointer<QSettings>(new QSettings(settingsFile, QSettings::IniFormat));
   
}

int Settings::crMaxBufferSize() const
{
    int result = m_settings->value(key_cr_maxBufferSize, default_cr_maxBufferSize).toInt();
    return result;
}

void Settings::setCrMaxBufferSize(int crMaxBufferSize)
{
    if (this->crMaxBufferSize() != crMaxBufferSize)
    {
        m_settings->setValue(key_cr_maxBufferSize, crMaxBufferSize);
        emit crMaxBufferSizeChanged();
    }
}

bool Settings::crUseEolPattern() const
{
    bool result = m_settings->value(key_cr_useEolPattern, default_cr_useEolPattern).toBool();
    return result; 
}

void Settings::setCrUseEolPattern(bool crUseEolPattern)
{
    m_settings->setValue(key_cr_useEolPattern, crUseEolPattern);
    emit crUseEolPatternChanged();
}

QString Settings::crEolPattern() const
{
    QString result = m_settings->value(key_cr_eolPattern, default_cr_eol_pattern).toString();
    return result; 
}

void Settings::setCrEolPattern(const QString &crEolPattern)
{
     m_settings->setValue(key_cr_eolPattern, crEolPattern);
     emit crEolPatternChanged();
}

bool Settings::crUseCategoryPattern() const
{
    bool result = m_settings->value(key_cr_useCategoryPattern, default_cr_useCategoryPattern).toBool();
    return result;
}

void Settings::setCrUseCategoryPattern(bool crUseCategoryPattern)
{
    m_settings->setValue(key_cr_useCategoryPattern, crUseCategoryPattern);
    emit crUseCategoryPatternChanged();
}

QString Settings::crCategoryPattern() const
{
    QString result = m_settings->value(key_cr_categoryPattern, default_cr_category_pattern).toString();
    return result; 
}

void Settings::setCrCategoryPattern(const QString &crCategoryPattern)
{
    m_settings->setValue(key_cr_categoryPattern, crCategoryPattern);
    emit crCategoryPatternChanged();
}

Settings::CrInputMode Settings::crInputMode() const
{
    CrInputMode result = m_settings->value(key_cr_inputMode, default_crInputMode).value<CrInputMode>();
    return result;
}

void Settings::setCrInputMode(const CrInputMode &crMode)
{
    m_settings->setValue(key_cr_inputMode, crMode);
    emit crInputModeChanged();
}

quint16 Settings::crTcpServerPort() const
{
    quint16 result = m_settings->value(key_cr_tcpServerPort, default_cr_tcpServerPort).value<quint16>();
    return result;
}

void Settings::setCrTcpServerPort(const quint16 &crTcpServerPort)
{
    m_settings->setValue(key_cr_tcpServerPort, crTcpServerPort);
    emit crTcpServerPortChanged();
}

QString Settings::crTcpClientHostAddress() const
{
    QString result = m_settings->value(key_cr_tcpClientHostAddress, default_cr_tcpClientHostAddress).toString();
    return result;
}

void Settings::setCrTcpClientHostAddress(const QString &crTcpClientHostAddress)
{
    if (this->crTcpClientHostAddress() != crTcpClientHostAddress)
    {
        m_settings->setValue(key_cr_tcpClientHostAddress, crTcpClientHostAddress);
        emit crTcpClientHostAddressChanged();
    }
}

quint16 Settings::crTcpClientPort() const
{
    quint16 result = m_settings->value(key_cr_tcpClientPort, default_cr_tcpClientPort).value<quint16>();
    return result;
}

void Settings::setCrTcpClientPort(const quint16 &crTcpClientPort)
{
    m_settings->setValue(key_cr_tcpClientPort, crTcpClientPort);
    emit crTcpClientPortChanged();
}

QString Settings::crSerialPort() const
{
    QString result = m_settings->value(key_cr_serialPort, default_cr_serialPort).toString();
    return result;
}

void Settings::setCrSerialPort(const QString &crSerialPort)
{
    m_settings->setValue(key_cr_serialPort, crSerialPort);
    emit crSerialPortChanged();
}

QStringList Settings::crAvailableSerialPorts() const
{
    QStringList result;
    QList<QSerialPortInfo> serPortInfos = QSerialPortInfo::availablePorts();
    
    foreach (auto info, serPortInfos)
    {
        result.append(info.systemLocation() + " - " + info.description());
    }
    
    return result;
}

bool Settings::otherAutoType() const
{
    bool result = m_settings->value(key_other_autoType, default_other_autoType).toBool();
    return result;
}

void Settings::setOtherAutoType(bool otherAutoType)
{
    m_settings->setValue(key_other_autoType, otherAutoType);
    emit otherAutoTypeChanged();
}


quint32 Settings::otherDelayAfterFocusLast() const
{
    quint32 result = m_settings->value(key_other_delayAfterFocusLast, default_other_delayAfterFocusLast).toUInt();
    return result;
}

void Settings::setOtherDelayAfterFocusLast(quint32 value)
{
    m_settings->setValue(key_other_delayAfterFocusLast, value);
    emit otherDelayAfterFocusLastChanged();
}

int Settings::otherSleepAfterEol() const
{
    int result = m_settings->value(key_other_sleepAfterEol, default_other_sleepAfterEol).toInt();
    return result;
}

void Settings::setOtherSleepAfterEol(int milliseconds)
{
    m_settings->setValue(key_other_sleepAfterEol, milliseconds);
    emit otherSleepAfterEolChanged();
}


