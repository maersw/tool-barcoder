/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import ThisApp 1.0 as App
import Lib.Util 1.0 as LibUtil

ApplicationWindow {

    visible: true
    width: 720
    height: 512
    title: qsTr("Barcoder")

    Item{
        id: root
        anchors.fill: parent
        
        Header{
            id: head
            
            anchors.top: root.top
            anchors.left: root.left
            
            receiver: receiver
            settings: drawer.settings
            drawer: drawer
            
            onDataReceived: {
                receiver.process(data)
            }
            
            onAutoTypeChanged: {
                if (autoType){
                    keyboardSim.focusLastWindow(drawer.settings.otherDelayAfterFocusLast)
                }
            }
            
            onTypeAll: {
                keyboardSim.focusLastWindow()
                
                var allCategories = foot.allCategories(drawer.settings.otherDelayAfterFocusLast);
                
                var l = allCategories.length
                
                for (var i = 0; i < l; ++i)
                {
                    var cat = allCategories[i];
                    var data = content.dataByCategory(cat);
                    
                    keyboardSim.preprocessAndSend(data); 
                } 
            }
            
            onTypeCategory: {
                keyboardSim.focusLastWindow(drawer.settings.otherDelayAfterFocusLast)
                
                var cat = foot.currentCategory;
                var data = content.dataByCategory(cat);
                
                keyboardSim.preprocessAndSend(data);
            }
        }
    
        Content{
            id: content
            
            currentCategory: foot.currentCategory
            
            anchors.top: head.bottom
            anchors.left: root.left
            anchors.right: root.right
            anchors.bottom: foot.top
        }

        Footer{
            id: foot
    
            anchors.left: root.left
            anchors.right: root.right
            anchors.bottom: root.bottom
        }
    }
    
    App.Receiver{
        id: receiver
        settings: drawer.settings
        
        readonly property string defaultCategory: "--"
        
        property var dataByCategory: {"": ""};
        
        onDataReceived: {
            foot.setData(category, data)
            content.setData(category, data)
            
            if (head.autoType)
            {
                keyboardSim.sendString(data);
            }
        }
        
        onCategoryChanged: {
            foot.activateCategory(category)
            content.show(category)
        }
    }
    
    LibUtil.KeyboardSimulator{
        id: keyboardSim
        
        function preprocessAndSend(str)
        {
            var eol = "\n";
            var parts = str.split(eol)
            
            var sleepTime = drawer.settings.otherSleepAfterEol
            
            for (var i = 0; i < parts.length; ++i)
            {
                var part = parts[i];
                keyboardSim.sendString(part)
                keyboardSim.sendString(eol)
                
                HmiRoot.msleep(sleepTime)
            }
            
        }
    }
    
    Settings{
        id: drawer
    }
    
}



