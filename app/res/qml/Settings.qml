/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import ThisApp 1.0 as App

Drawer{
    id: root
    property alias settings: settingsId

    
    App.Settings{
        id: settingsId
        
        function escapeChars(str)
        {
            var result = str
            .replace(/\n/g, "\\n")
            .replace(/\r/g, "\\r")
            .replace(/\t/g, "\\t")
            ;
            
            return result;
        }
        
        function unescapeChars(str)
        {
            var result = str
            .replace(/\\n/g, "\n")
            .replace(/\\r/g, "\r")
            .replace(/\\t/g, "\t")
            ;
            
            return result;
        }
    }
    
    
    GridLayout{
        id: grid
        columns: 2
        
        ////////////////////////////////////////////////////////////
        // Row 1
        ////////////////////////////////////////////////////////////
        Text{
            text: qsTr("Max. buffer size")
        }

        TextField {
            id: crMaxBufferSize
            width: 113
            height: 40
            text: settingsId.crMaxBufferSize
            inputMask: "dddd"
        }
        
        ////////////////////////////////////////////////////////////
        // Row 2
        ////////////////////////////////////////////////////////////
        CheckBox{
            id: cruseEolPattern
            text: qsTr("Use EOL pattern")
            checked: settingsId.crUseEolPattern
        }
        
        TextField{
            id: crEolPattern
            text: settingsId.escapeChars(settingsId.crEolPattern)
        }
        
        ////////////////////////////////////////////////////////////
        // Row 3
        ////////////////////////////////////////////////////////////
        CheckBox{
            id: crUseCategoryPattern
            text: qsTr("Use category pattern")
            checked: settingsId.crUseCategoryPattern
        }
        
        TextField{
            id: crCategoryPattern
            text: settingsId.escapeChars(settingsId.crCategoryPattern)
        }
        
        ////////////////////////////////////////////////////////////
        // Row 4
        ////////////////////////////////////////////////////////////
        
        Text{
            text: qsTr("Input mode")
        }

        ComboBox {
            id: crInputMode
            model: [qsTr("Keyboard only"), qsTr("Tcp server"), qsTr("Tcp client"), qsTr("Serial")]
            currentIndex: settingsId.crInputMode
            
            TextField{
                id: crTcpServerPort
                text: settingsId.crTcpServerPort
                width: 75
                anchors.left: crInputMode.right
                visible: crInputMode.currentIndex === App.Settings.TcpServer
            }
            
            TextField{
                id: crTcpClientPort
                width: crTcpServerPort.width
                text: settingsId.crTcpClientPort
                anchors.left: crInputMode.right
                visible: crInputMode.currentIndex === App.Settings.TcpClient
            }
            
            TextField{
                id: crTcpClientHostAddress
                width: crTcpServerPort.width
                text: settingsId.crTcpClientHostAddress
                anchors.left: crTcpClientPort.right
                visible: crInputMode.currentIndex === App.Settings.TcpClient
            }
            
            TextField{
                id: crSerialPort
                width: crTcpServerPort.width
                text: settingsId.crSerialPort
                anchors.left: crInputMode.right
                visible: crInputMode.currentIndex === App.Settings.Serial
            }
        }
     
        ////////////////////////////////////////////////////////////
        // Row 5
        ////////////////////////////////////////////////////////////
        Text{
            text: qsTr("Delay after focus last")
        }

        TextField {
            id: otherDelayAfterFocusLast
            width: 113
            height: 40
            text: settingsId.otherDelayAfterFocusLast
            //inputMask: "dddd"
        }
        ////////////////////////////////////////////////////////////
        // Row 6
        ////////////////////////////////////////////////////////////

        Text{
            text: qsTr("Sleep ms after eol")
        }

        TextField {
            id: otherSleepAfterEol
            width: 113
            height: 40
            text: settingsId.otherSleepAfterEol
            //inputMask: "dddd"
        }
        
        ////////////////////////////////////////////////////////////
        // Row 7
        ////////////////////////////////////////////////////////////
        Button{
            text: qsTr("Save")
            onClicked: {
                settingsId.crMaxBufferSize        = crMaxBufferSize.text
                settingsId.crUseEolPattern        = cruseEolPattern.checked
                settingsId.crEolPattern           = settingsId.unescapeChars(crEolPattern.text)
                settingsId.crUseCategoryPattern   = crUseCategoryPattern.checked
                settingsId.crCategoryPattern      = settingsId.unescapeChars(crCategoryPattern.text)
                settingsId.crInputMode            = crInputMode.currentIndex;
                settingsId.crTcpServerPort        = crTcpServerPort.text
                settingsId.crTcpClientPort        = crTcpClientPort.text
                settingsId.crTcpClientHostAddress = crTcpClientHostAddress.text
                settingsId.crSerialPort           = crSerialPort.text
                settingsId.otherDelayAfterFocusLast = otherDelayAfterFocusLast.text
                settingsId.otherSleepAfterEol     = otherSleepAfterEol.text
            }
        }
        
        Button{
            text: qsTr("Cancel")
            onClicked: {
                root.close()
            }
        }
        
        ////////////////////////////////////////////////////////////
        // Row 
        ////////////////////////////////////////////////////////////

    }
}
