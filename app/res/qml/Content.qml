/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: root
    
    
    property string currentCategory: ""
    
    onCurrentCategoryChanged: {
        show(currentCategory)
    }
    
    
    function dataByCategory(category)
    {
        var result = "";
        if (category in internal.txtByCat)
        {
            result = internal.txtByCat[category].text;
        }
        
        return result;
    }
    
    function setData(category, data)
    {
        if (!(category in internal.txtByCat))
        {
            var c = comp.createObject(root)
            c.objectName = category
            internal.txtByCat[category] = c;
        }
        
        var cc = internal.txtByCat[category];
        //cc.text = data + cc.text;
        cc.text += data
    }
    
    function show(category)
    {
        // ensure textfiel exists
        setData(category, "");
        
        var ks = Object.keys(internal.txtByCat);
        
        var l = ks.length;
        
        for (var i = 0; i < l; ++i)
        {
            var k = ks[i]
            var item = internal.txtByCat[k]
            item.visible = (item.objectName === category) ? true : false
        }
        
    }

    Item{
        id: internal
        
        property var txtByCat: {"": comp.createObject(root)}

    }
    
    Component{
        id: comp

        ScrollView {
            id: flick
            
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            
            anchors.fill: parent
            property alias text: txt.text
            TextArea {
                id: txt
                
                selectByMouse: true
                anchors.fill: parent
            }    
        }
    }
}
