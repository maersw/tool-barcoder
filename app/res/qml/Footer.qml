/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import ThisApp 1.0 as App

TabBar {
    
    id: root
    
    Component{
        id: dataComp
        TabButton {
            property string data: ""
        }
    }
    
    Component.onCompleted: {
        // add default category
        internal.addTab("")
    }
    
    
    function allCategories()
    {
        var categories = [];
        
        var l = root.contentChildren.length
        
        for (var i = 0; i < l; ++i)
        {
            var cat = root.contentChildren[i].objectName
            categories.push(cat)
        }

        return categories
    }
    
    property string currentCategory: {
        var tab = internal.getTab(root.currentIndex)
        
        if (tab)
        {
            return tab.objectName;
        }
        else
        {
            return ""
        }
    }

    function activateCategory(cat){
        
        var ind = internal.addTab(cat);
        root.currentIndex = -1;
        root.currentIndex = ind;
        
        return ind
    }
    
    function setData(cat, data){
        
        var ind = activateCategory(cat)
        
        var tab = internal.getTab(ind)
        
        tab.data += data;

    }

    Item{
        id: internal
        
        property var tabByCategory: {"": ""};
        
        function getTabIndexByCategory(cat){
            var l = root.contentChildren.length
            
            var i = 0;
            for (; i < l; ++i)
            {
                var ch = root.contentChildren[i]
                if (ch.objectName === cat)
                {
                    break;
                }
            }
            
            if (i >= l)
            {
                // not found
                i = -1;
            }
            
            return i;
        }
        
        
        function addTab(cat)
        {
            var ind = getTabIndexByCategory(cat);
            
            
            if (ind < 0)
            {
                var item = dataComp.createObject(root, {"text": cat});
                item.objectName = cat
                root.addItem(item)
                
                ind = getTabIndexByCategory(cat);
            }
            
            return ind;
        }
        
        function getTab(index)
        {
            return root.contentChildren[index]
        }

    }

}
