/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import ThisApp 1.0 as App



Row{
    id: root
    
    property App.Receiver receiver;
    property App.Settings settings;
    property Drawer drawer
    
    property alias autoType: buttonAutoType.checked
    
    signal dataReceived(string data)
    signal typeCategory();
    signal typeAll()
    
    Item{
        id: internal
        readonly property int toolTipDelay: 1000
        readonly property int toolTipTimeout: 5000
    }
    
    Button{
        text: qsTr("Settings")
        onClicked: root.drawer.open()
    }
    
    Button{
        id: buttonStart
        text: checked ? qsTr("Stop") : qsTr("Start")
        checkable: true
        
        onCheckedChanged: {
            if (checked)
            {
                root.receiver.start()
            }
            else
            {
                root.receiver.stop()
            }
        }
    }
    
    TextField{
        id: textInput
        
        // disable textfield when autotype is active to
        // prevent input loop: when textfield has focus
        // entered text will be auto-entered again into the 
        // text field
        enabled: !buttonAutoType.checked

        Keys.onPressed: {
            switch(event.key)
            {
            case Qt.Key_Enter:
            case Qt.Key_Return:
                
                var text = textInput.text
                
                if (root.settings.crUseEolPattern)
                {
                    text += root.settings.crEolPattern;
                }

                textInput.text = ""
                dataReceived(text)
                break;
                
            default:
                break;
            }
        }
        
    }
    
    Button{
        text: qsTr("Type category")
        onClicked: root.typeCategory()
        
        ToolTip.delay: internal.toolTipDelay
        ToolTip.timeout: internal.toolTipTimeout
        ToolTip.visible: hovered
        ToolTip.text: qsTr("Active last window and type text of current category")
    }
    
    Button{
        text: qsTr("Type all")
        onClicked: root.typeAll()
        
        ToolTip.delay: internal.toolTipDelay
        ToolTip.timeout: internal.toolTipTimeout
        ToolTip.visible: hovered
        ToolTip.text: qsTr("Active last window and type text of all categories")
    }
    
    Button{
        id: buttonAutoType
        text: qsTr("Autotype")
        checkable: true
        
        onCheckedChanged: {
            if (checked){
                buttonStart.checked = true
            }
                
        }
        
        ToolTip.delay: internal.toolTipDelay
        ToolTip.timeout: internal.toolTipTimeout
        ToolTip.visible: hovered
        ToolTip.text: qsTr("Simulate key press immediately when data is received")
    }
}
