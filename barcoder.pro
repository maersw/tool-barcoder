QT += quick serialport

CONFIG += c++11

# to be updated only for tagged commits!
VERSION = 0.0.0

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


RESOURCES_DIR = $$_PRO_FILE_PWD_/app/res
DEFINES += RESOURCE_BASE_DIR=\"$$quote(\\\"$$RESOURCES_DIR\\\")\"

HEADERS += \
    app/src/guiroot.hpp \
    app/src/logging.hpp \
    app/src/settings.hpp \
    app/src/receiver.hpp \
    app/src/barcodereceiver/barcodereceiver.hpp           \
    app/src/barcodereceiver/barcodereceiver_logging.hpp   \
    app/src/barcodereceiver/barcodereceiver_serial.hpp    \
    app/src/barcodereceiver/barcodereceiver_tcpclient.hpp \
    app/src/barcodereceiver/barcodereceiver_tcpserver.hpp \


SOURCES += \
    app/src/main.cpp \
    app/src/guiroot.cpp \
    app/src/logging.cpp \
    app/src/settings.cpp \
    app/src/receiver.cpp \
    app/src/barcodereceiver/barcodereceiver.cpp           \
    app/src/barcodereceiver/barcodereceiver_logging.cpp   \
    app/src/barcodereceiver/barcodereceiver_serial.cpp    \
    app/src/barcodereceiver/barcodereceiver_tcpclient.cpp \
    app/src/barcodereceiver/barcodereceiver_tcpserver.cpp \


DISTFILES += \
    $$files(app/res/qml/*.qml, true)

RESOURCES += app/res/resources.qrc


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


libUtil += kmp
libUtil += keyboardSimulator
libUtil += qt
include(submodules/lib-utility/lib.utility.pri)

win32-msvc*: {
    LIBS += User32.lib
}


DEFINES += "BUILD_MODE_RELEASE=1"
DEFINES += "BUILD_MODE_DEBUG=2"

CONFIG(release, release|debug){
    DEFINES += "BUILD_MODE=BUILD_MODE_RELEASE"
    SETTINGS -= DISABLE_QML_CACHE

    win32:{
        arch="64"
        !contains(QMAKE_TARGET.arch, x86_64) {
            arch="32"
        }
    
        compiler=""
        win32-g++: compiler="mingw"
        win32-msvc*: compiler="msvc"
    
        deployDir="$${TARGET}_$${VERSION}_$${compiler}$${arch}"
    
        QMAKE_POST_LINK += $$m_winDeploy("$$deployDir")
    
        QMAKE_POST_LINK += $$m_copyDir($$RESOURCES_DIR, $$deployDir/res)
    }
}
else{
    DEFINES += "BUILD_MODE=BUILD_MODE_DEBUG"
    SETTINGS += DISABLE_QML_CACHE

}

###########################################################
# DISABLE_QML_CACHE
###########################################################
# Disables the qml caching feature. 
# Note: 
# When the 'else' branch below is active qml cache
# can still be disabled by setting environment variable
# QML_DISABLE_DISK_CACHE to 1.
contains(SETTINGS, DISABLE_QML_CACHE){
    message("SETTINGS += DISABLE_QML_CACHE")
    DEFINES += "DISABLE_QML_CACHE=1"
    
}
else{
    message("SETTINGS -= DISABLE_QML_CACHE")
    DEFINES += "DISABLE_QML_CACHE=0"
}







